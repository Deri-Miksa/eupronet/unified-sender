#pragma once
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <LittleFS.h>

#define MODE_COUNT 2

enum SendMethod
{
    GET,
    POST
};

enum Mode
{
    Start,
    Finish
};

struct Config
{
    String value;
    const char *name;
    const char *displayName;

    Config(const char *name, const char *displayName) : name(name), displayName(displayName) {}
};

// Sends the given data to the desired url
StaticJsonDocument<256> SendData(HTTPClient &http, WiFiClientSecure &client, const SendMethod &method, const String &url, const String &data);

// Reads the configurations from the file system
void GetConfig(Config *configs, size_t count);

// Saves the configuration values to the file system
void SaveConfig(Config *configs, size_t count);

// Blink the given led for the given amount of time (ms)
void blink(int pin, int ms);