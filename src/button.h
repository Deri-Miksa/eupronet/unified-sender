#pragma once
#include <Arduino.h>

enum PrintState
{
    None,
    Started,
    Discarded,
    Finished
};

class Button
{
public:
    Button(uint8_t pin);
    bool isReleased();
    void readState();
    void init(uint8_t mode);
    bool pressedFor(size_t ms) const;
    bool pressedFor(size_t msMin, size_t msMax) const;
    size_t pressLength() const { return _pressLength; }

private:
    uint8_t _pin;
    bool _isPressed;
    bool _wasPressed;
    bool _isReleased;
    size_t _pressStart;
    size_t _pressLength;
};

struct ColorButtonState
{
    int colorID;
    const char *colorName;
    PrintState printState = PrintState::None;
    size_t pressCount = 0;
};