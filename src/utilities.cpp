#include "utilities.h"

StaticJsonDocument<256> SendData(HTTPClient &http, WiFiClientSecure &client, const SendMethod &method, const String &url, const String &data)
{
    http.begin(client, url);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = (method == GET ? http.GET() : http.POST(data));
    String payload = http.getString();
    http.end();

    StaticJsonDocument<256> doc;

    if (httpResponseCode != 200)
        return doc;

    DeserializationError err = deserializeJson(doc, &payload[0]);
    if (err)
        doc.clear();

    return doc;
}

void GetConfig(Config *configs, size_t count)
{
    if (LittleFS.begin())
    {
        if (LittleFS.exists("/config.json"))
        {
            Serial.println("Reading config file...");
            File configFile = LittleFS.open("/config.json", "r");
            if (configFile)
            {
                size_t size = configFile.size();
                // Allocate a buffer to store contents of the file.
                std::unique_ptr<char[]> buf(new char[size]);

                configFile.readBytes(buf.get(), size);
                StaticJsonDocument<JSON_OBJECT_SIZE(2)> doc;

                DeserializationError error = deserializeJson(doc, buf.get());
                if (!error)
                {
                    for (size_t i = 0; i < count; i++)
                    {
                        if (doc.containsKey(configs[i].name))
                            configs[i].value = doc[configs[i].name].as<String>();
                    }
                }
                else
                    Serial.println("Failed to parse json");
                configFile.close();
            }
            else
                Serial.println("Failed to open file!");
        }
        else
            Serial.println("Config file doesn't exist");
    }
    else
        Serial.println("Failed to mount FS");
}

void SaveConfig(Config *configs, size_t count)
{
    Serial.println("Saving config...");
    StaticJsonDocument<JSON_OBJECT_SIZE(2) + 200> doc;

    for (size_t i = 0; i < count; i++)
        doc[configs[i].name] = configs[i].value;

    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile)
        Serial.println("Failed to open config file for writing!");

    serializeJson(doc, configFile);
    serializeJson(doc, Serial);
    configFile.close();
}

void blink(int pin, int ms)
{
    int initialState = digitalRead(pin);
    digitalWrite(pin, !initialState);
    delay(ms);
    digitalWrite(pin, initialState);
}