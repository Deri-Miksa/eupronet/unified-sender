#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <alloca.h>

#include "utilities.h"
#include "button.h"

#define BUTTON_COUNT 4
#define CONFIG_COUNT 2
#define SWITCH 0x01

// I/O
int leds[] = {D0, D1, D3, D4};
int mode = Mode::Start;
Button colorButtons[BUTTON_COUNT] = {D5, D6, D7, D8};
ColorButtonState colorButtonStates[BUTTON_COUNT] = {
    {1, "red"},
    {2, "green"},
    {3, "blue"},
    {4, "yellow"}};
Button switchButton(D2);

// API connection
HTTPClient http;
WiFiClientSecure client;
String host = "https://api.derimiksa.hu";
const char *fingerprint = "21 E8 52 6A 2A 18 A9 52 52 15 2E B4 D4 ED 9B 59 40 9C F0 5B";
String token;

// WifiManager config
Config configs[CONFIG_COUNT] = {
    {"apiUsername", "API username"},
    {"apiPassword", "API password"}};
bool shouldSaveConfig = false;

void configModeCallback(WiFiManager *wm)
{
    Serial.println("Entered config mode");
    Serial.println(WiFi.softAPIP());
    Serial.println(wm->getConfigPortalSSID());
    blink(leds[2], 1000);
}

void setup()
{
    Serial.begin(115200);
    while (!Serial)
        ;

    // I/O init
    for (int i = 0; i < BUTTON_COUNT; i++)
    {
        colorButtons[i].init(INPUT);
        pinMode(leds[i], OUTPUT);
        digitalWrite(leds[i], LOW xor SWITCH);
    }
    switchButton.init(INPUT);

    // Network config
    client.setFingerprint(fingerprint);
    client.setTimeout(15000);
    http.setReuse(true);

    // SPIFFS.format(); // For testing!
    GetConfig(configs, CONFIG_COUNT);

    WiFiManager wifiManager;
    // wifiManager.resetSettings(); // For testing!
    wifiManager.setDebugOutput(false);
    wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 1, 1), IPAddress(192, 168, 1, 1), IPAddress(255, 255, 255, 0));
    wifiManager.setAPCallback(configModeCallback);
    wifiManager.setSaveConfigCallback([]()
                                      { shouldSaveConfig = true; });

    // Create parameters from configs
    WiFiManagerParameter *params = (WiFiManagerParameter *)alloca(CONFIG_COUNT * sizeof(WiFiManagerParameter));
    for (size_t i = 0; i < CONFIG_COUNT; i++)
    {
        Config &config = configs[i];
        params[i] = WiFiManagerParameter(config.name, config.displayName, config.value.c_str(), 100);
        wifiManager.addParameter(&params[i]);
    }

    // Connect to wifi
    if (!wifiManager.autoConnect())
    {
        Serial.println("Failed to connect and hit timeout! Reseting...");
        blink(leds[0], 1000); // Blink red
        ESP.restart();
    }

    Serial.println("Successfully connected to " + WiFi.SSID());
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    // Read the parameters
    for (size_t i = 0; i < CONFIG_COUNT; i++)
        configs[i].value = params[i].getValue();

    if (shouldSaveConfig)
        SaveConfig(configs, CONFIG_COUNT);

    // Login to the api
    String data =
        (String) "{" +
        "\"username\": \"" + configs[0].value + "\", " +
        "\"password\": \"" + configs[1].value + "\"" +
        "}";

    auto loginResponse = SendData(http, client, SendMethod::POST, host + "/login", data);
    if (!loginResponse.isNull())
    {
        if (loginResponse["status"] == "ok")
        {
            token = loginResponse["token"].as<String>();
            Serial.print("Successfull login! Token: ");
            Serial.println(token);
        }
        else
        {
            Serial.print("Login failed! ");
            Serial.println((const char *)loginResponse["msg"]);

            blink(leds[0], 1000); // Blink red
            ESP.restart();
        }
    }

    // Everything successfull
    blink(leds[1], 1000); // Blink green
}

void loop()
{
    for (size_t i = 0; i < BUTTON_COUNT; i++)
    {
        Button &button = colorButtons[i];
        ColorButtonState &state = colorButtonStates[i];

        button.readState();
        if (button.isReleased())
        {
            // Start mode short press
            if (mode == Mode::Start && button.pressedFor(500))
            {
                state.pressCount++;
                state.printState = PrintState::Started;
                Serial.println(String("Started printing ") + state.colorName);
            }
            // Finish mode short press
            else if (mode == Mode::Finish && button.pressedFor(500) && state.pressCount > 0)
            {
                state.pressCount++;
                state.printState = PrintState::Finished;
                Serial.println(String("Finished printing ") + state.colorName);
            }
            // Long press
            else if (button.pressedFor(500, -1) && state.pressCount > 0)
            {
                state.pressCount--;
                state.printState = PrintState::Discarded;
                Serial.println(String("Discarded ") + state.colorName);
            }

            digitalWrite(leds[i], (state.pressCount > 0 ? HIGH : LOW) xor SWITCH);

            if (state.printState != PrintState::None)
            {
                String data =
                    (String) "{" +
                    "\"token\": \"" + token + "\", " +
                    "\"state\": \"" + state.printState + "\", " +
                    "\"color\": \"" + state.colorID + "\"" +
                    "}";

                auto printResponse = SendData(http, client, SendMethod::POST, host + "/insert/print", data);
                if (!printResponse.isNull())
                {
                    if (printResponse["status"] != "ok")
                    {
                        Serial.println("Upload failed! Message:");
                        Serial.println((const char *)printResponse["msg"]);

                        blink(leds[0], 1000); // Blink red
                        ESP.restart();
                    }
                }
                state.printState = PrintState::None;
            }
        }
    }

    switchButton.readState();
    if (switchButton.isReleased())
    {
        mode = (++mode) % MODE_COUNT;
        switch (mode)
        {
        case Mode::Start:
            blink(leds[0], 500);
            Serial.println("Switched to start mode");
            break;
        case Mode::Finish:
            blink(leds[1], 500);
            Serial.println("Switched to finish mode");
            break;
        }
    }

    delay(10);
}
