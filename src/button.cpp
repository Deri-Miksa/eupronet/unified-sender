#include "button.h"

Button::Button(uint8_t pin)
    : _pin(pin), _isPressed(false), _wasPressed(false), _isReleased(false), _pressStart(0), _pressLength(0)
{
}

void Button::init(uint8_t mode)
{
    pinMode(_pin, mode);
}

bool Button::pressedFor(size_t ms) const
{
    if (_pressLength < ms) return true;
    return false;
}

bool Button::pressedFor(size_t msMin, size_t msMax) const
{
    if (_pressLength > msMin && _pressLength < msMax) return true;

    return false;
}

void Button::readState()
{
    _isPressed = digitalRead(_pin) == HIGH;
    _isReleased = !_isPressed && _wasPressed;
    if (_isReleased)
        _pressLength = millis() - _pressStart;
    else
        _pressStart = millis();

    _wasPressed = _isPressed;
}

bool Button::isReleased()
{
    return _isReleased;
}